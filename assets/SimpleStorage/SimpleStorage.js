var _sleep=function(sec){var dt=new Date();dt.setTime(dt.getTime()+sec*1000);while(new Date().getTime()<dt.getTime());}
var storageOutput={"contracts":{"../sol/SimpleStorage.sol:SimpleStorage":{"abi":"[{\"constant\":true,\"inputs\":[],\"name\":\"get\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"data\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"number\",\"type\":\"uint256\"}],\"name\":\"set\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]","bin":"6080604052348015600f57600080fd5b5060ab8061001e6000396000f3fe6080604052348015600f57600080fd5b506004361060325760003560e01c806360fe47b11460375780636d4ce63c146053575b600080fd5b605160048036036020811015604b57600080fd5b5035606b565b005b60596070565b60408051918252519081900360200190f35b600055565b6000549056fea265627a7a723158204520ae186d012cce0a4393cda9018a2b17372af69340df400bbe1018f860af1e64736f6c63430005100032"}},"version":"0.5.16+commit.9c3226ce.Linux.g++"};
var storageContractAbi = storageOutput.contracts['../sol/SimpleStorage.sol:SimpleStorage'].abi;
var storageContract = eth.contract(JSON.parse(storageContractAbi));
var storageBinCode = "0x" + storageOutput.contracts['../sol/SimpleStorage.sol:SimpleStorage'].bin;
personal.unlockAccount(personal.listAccounts[0],'node0');
var deployTransationObject = { from: eth.accounts[0], data: storageBinCode, gas: 10000000 };
var storageInstance = storageContract.new(deployTransationObject)
var timeout=60; while(timeout>0){console.log(timeout);_sleep(1);timeout--;}
var storageAddress = eth.getTransactionReceipt(storageInstance.transactionHash).contractAddress;
var storage = storageContract.at(storageAddress);
storage.sleep=_sleep
storage.tx=storageInstance.transactionHash
