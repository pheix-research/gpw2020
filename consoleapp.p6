#! /usr/bin/env perl6

use Net::Ethereum;

use lib './lib';
use Eth::Minimalistic;

my UInt $value_to_set = @*ARGS[1].UInt if @*ARGS[1].defined;

die 'Usage: ./consoleapp.p6 tx_hash [valiue_to_set]' unless @*ARGS[0].defined;

# Create blockchain object
my $eth = Eth::Minimalistic.new(
    abi       => './assets/solcoutput/SimpleStorage.abi'.IO.slurp,
    apiurl    => 'http://127.0.0.1:8540',
    sctx      => @*ARGS[0] || ( "0x" ~ q{0} x 64 ),
    unlockpwd => 'node0'
);

# Initialize ethereum object and die if init fails
die 'Ethereum node is down' unless $eth.initialize;

if $value_to_set.defined {
    $eth.ethobj.personal_unlockAccount;
    my $status = $eth.set_data(
        number => $value_to_set,
        waittx => False
    );
    (
        'data value <' ~ $value_to_set ~
        '> is set via tx <' ~ $status<txhash>
    ).say;
}

('actual data value: ' ~ $eth.get_data ).say;
